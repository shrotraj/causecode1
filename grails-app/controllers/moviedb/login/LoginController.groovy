package moviedb.login

import moviedb.Role
import moviedb.User
import moviedb.UserRole


class LoginController {

    def index() {


    }

    def loginValidate() {
        Map loginData = [userName: params.username, password: params.password]
        println(User.findByUserNameAndPassword(params.username, params.password))
        if(User.findByUserNameAndPassword(params.username, params.password)) {
            redirect(controller: "movie", action: "home")
        }
        else {
            redirect(controller: "login", action:"index")
        }

    }

    def register() {

    }

    def registerUser() {
        Map registrationData = [username: params.username, password: params.password, email: params.email]
        User userData = new User(registrationData)
        if (userData.save()) {
           UserRole.create(userData, Role.findByAuthority('ROLE_COMMON'))
            }

        respond([success: true])
    }
}
