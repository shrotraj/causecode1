package moviedb

import grails.test.mixin.TestFor
import moviedb.login.LoginController
import spock.lang.Specification
import grails.test.mixin.Mock




/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(LoginController)
@Mock([User, Role, UserRole])
class LoginControllerSpec1 extends Specification {

    def setup() {

    }

    def cleanup() {
    }

    void "test saving a register user"() {
        when:"user clicks on register"
        controller.params.username = 'shrot'
        controller.params.email = 'shrot@gmail.com'
        controller.params.password = 'test'
        controller.request.method = 'POST'
        controller.registerUser()
        then:
        model.success == true
        User.count() == 1
        User.findByUsername('shrot') != null
    }
}
