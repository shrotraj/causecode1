import moviedb.*

class BootStrap {

    def init = { servletContext ->
        User admin = new User(username:'admin', password:'secret', email: 'admin@test.com' , enabled:true).save()
        User john = new User(username:'john', password:'secret', email: 'john@test.com', enabled:true).save()
        User jane = new User(username:'jane', password:'secret', email: 'jane@test.com', enabled:true).save()

        Role administrative = new Role(authority: 'ROLE_ADMINISTRATIVE').save()
        Role common = new Role(authority: 'ROLE_COMMON').save()

        UserRole.create(admin, administrative)
        UserRole.create(admin, common)
        UserRole.create(john, common)

    }
    def destroy = {
    }
}
