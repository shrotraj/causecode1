package moviedb

import grails.test.mixin.TestFor
import moviedb.movie.MovieController
import spock.lang.Specification
import grails.test.mixin.Mock
/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@Mock([Movie,RateRequest,EditRequest])
@TestFor(MovieController)
class MovieControllerSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }


        void "testing movie controller options"() {
            given: 'Movie'
            Movie movie = new Movie(title: 'test', genre: 'horror', director: 'test', year: 2016, certificate: 'U', summary: 'This is a test movie instance.', rating: '5').save(flush: true)
            assert movie.id != null
            when:""
            controller.request.method = 'POST'
            controller.params.id = movie.id
            controller.params.director = 'updated director'
            controller.update()
            then: ''
            Movie updatedMovie = Movie.get(movie.id)
            updatedMovie.director == 'updated director'
    }

    void "test listMovie"() {
        given: 'Movie'
        Movie movie = new Movie(title: 'test', genre: 'horror', director: 'test', year: 2016, certificate: 'U', summary: 'This is a test movie instance.', rating: '5').save(flush: true)
        assert  movie.id != null
        when: 'User clicks on listMovie'
        Map response = controller.listMovies()
        then: 'server should list all movie records'
        response.movieData == [movie]
    }
    void "test updateList"(){
        given:'Movie'
        Movie movie = new Movie(title: 'test', genre: 'horror', director: 'test', year: 2016, certificate: 'U', summary: 'This is a test movie instance.', rating: '5').save(flush: true)
        assert  movie.id != null
        when: 'User clicks on updateMovie'
        Map response = controller.updateList()
        then: 'server should list all updated movie records'
        response.movieData == [movie]
    }

    void "test addMovies"(){
        when:"user clicks on addMovies"
        controller.params.title="Iron-Man 3"
        controller.params.genre="Action,fantasy"
        controller.params.director="hahahahaha"
        controller.params.year=2121
        controller.params.summary="this is a test summary for the movie iron-man 3"
        controller.params.certificate="U"
        controller.params.rating=''
        controller.request.method="POST"
        controller.index()

        then:"A movie record should be created"
        Movie.count() ==1
        Movie.findByTitle('Iron-Man 3') != null
    }

    void "test delete"(){
        given:'Movie'
        Movie movie = new Movie(title: 'test', genre: 'horror', director: 'test', year: 2016, certificate: 'U', summary: 'This is a test movie instance.', rating: '5').save(flush: true)
        assert  movie.id != null
        when:"user click on deleteMovies"
        controller.params.edit = movie.id
        controller.delete()
        then:'selected movie record is deleted'
        Movie.count() ==0
        Movie.findByTitle('test') == null

    }

    void "test requestPage"() {
        given: 'Movie'
        Movie movie = new Movie(title: 'test', genre: 'horror', director: 'test', year: 2016, certificate: 'U', summary: 'This is a test movie instance.', rating: '5').save(flush: true)
        assert  movie.id != null
        when: 'User clicks on listMovie'
        Map response = controller.requestPage()
        then: 'server should list all movie records'
        response.movieData == [movie]
    }

    void "test filter1"(){
        given: 'Movie'
        Movie movie = new Movie(title: 'test', genre: 'horror', director: 'test', year: 2016, certificate: 'U', summary: 'This is a test movie instance.', rating: '5').save(flush: true)
        assert  movie.id != null
        when: 'user clicks on filter1'
        controller.params.filter1='test'
        controller.filter1()
        controller.request.method="POST"
        then:"list of movie according to title serached is returned"
        model.movieData == [movie]
        view == '/movie/filtered'
    }
    void "test filter2"(){
        given: 'Movie'
        Movie movie = new Movie(title: 'test', genre: 'horror', director: 'test', year: 2016, certificate: 'U', summary: 'This is a test movie instance.', rating: '5').save(flush: true)
        assert  movie.id != null
        when: 'user clicks on filter2'
        controller.params.filter2='2016'
        controller.filter2()
        controller.request.method="POST"
        then:"list of movie according to year searched is returned"
        model.movieData == [movie]
        view == '/movie/filtered'
    }

    void "test filter3"(){
        given: 'Movie'
        Movie movie = new Movie(title: 'test', genre: 'horror', director: 'test', year: 2016, certificate: 'U', summary: 'This is a test movie instance.', rating: '5').save(flush: true)
        assert  movie.id != null
        when: 'user clicks on filter3'
        controller.params.filter3='U'
        controller.filter3()
        controller.request.method="POST"
        then:"list of movie according to certificate searched is returned"
        model.movieData == [movie]
        view == '/movie/filtered'
    }

    void "test rateMovies"() {
        given: 'Movie'
        Movie movie = new Movie(title: 'test', genre: 'horror', director: 'test', year: 2016, certificate: 'U', summary: 'This is a test movie instance.', rating: '5').save(flush: true)
        assert  movie.id != null
        when: 'User clicks on rateMovies'
        Map response = controller.rateMovies()
        then: 'server should list all movie records'
        response.movieData == [movie]
    }
}
