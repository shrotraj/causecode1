package moviedb

import grails.plugin.springsecurity.annotation.Secured


/**
 * Created by shrot on 19/6/17.
 */
class ScreenController {

    def authenticatedPage() {
        return 1
//        render "This is a authenticated only page"
    }
    @Secured(['ROLE_COMMON'])
    def commonPage() {
        render (view:'common')
    }
    @Secured(['ROLE_ADMINISTRATIVE'])
    def adminPage() {
        render "This is a royal role page"
    }

}
