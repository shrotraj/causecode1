<!DOCTYPE html>

<html xmlns:g="http://www.w3.org/1999/html"><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>Online Movie DataBase</title>
    <style>
body {
     background: url('http://webneel.com/daily/sites/default/files/images/daily/01-2016/25-the-nut-job-2-animation-movie-list-2016.jpg') no-repeat fixed center center;
    background-size: cover;
    font-family: Montserrat;
}

.logo {
    font-size:18px;
    text-align: center;
    width: 800px;
    height: 55px;
    background-color:white;
    border-radius: 5px;
    border-top: 5px solid #ff656c;
    border-bottom: 5px solid #ff656c;
    margin: 20px auto;
}

.movie-block {
    width: 90%;
    height: 400px;
    padding: 20px;
    background-color:#B2BABB;
    border-radius: 5px;
    border-top: 5px solid #ff656c;
    border-bottom: 5px solid #ff656c;
    margin: 50px auto;
    font-family: Montserrat;
    opacity: 0.9;
}

.movie-block h1 {
    text-align: center;
    color: #000;
    font-size: 18px;
    text-transform: uppercase;
    margin-top: 0;
    margin-bottom: 20px;
}

.movie-block input {
    display:table-cell;
    width: 100%;
    height: 35px;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #ccc;
    margin-bottom: 20px;
    margin-left: 60px;
    font-size: 14px;
    font-family: Montserrat;
    padding: 0 20px 0 50px;
    outline: none;
}
.movie-block button {
    width: 15%;
    height: 40px;
    background: #ff656c;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #e15960;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 14px;
    font-family: Montserrat;
    outline: none;
    cursor: pointer;


}





#submit {
    width: 15%;
    height: 40px;
    background: #ff656c;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #e15960;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 14px;
    font-family: Montserrat;
    outline: none;
    cursor: pointer;
    margin-left:20;
    float: right;

}

</style>
</head>

<body>


<div class="logo"><h3>Online Movie Database </h3></div>
<div class="movie-block">
    <h1>Submit movie detail</h1>

    <g:form controller = "movie" action = "index" method ="POST">
        <table>
            <tr><td width="30%"><label> Title </label> </td> <td width="70%" colspan="2"><input type = "text" name="title"></td></tr>
            <tr><td width="30%"><label> Genre </label> </td> <td width="70%"><input type = "text"  name="genre"></td></tr>
            <tr><td width="30%"><label> Director </label> </td> <td width="70%"> <input type = "text"  name="director"></td></tr>
            <tr><td width="30%"><label> Year of release</label></td><td width="70%"><input type = "text" name="year"></td></tr>
            <tr><td width="30%"><label> short summary </label></td> <td width="70%"> <input type ="text field" name="summary"></td></tr>
            <tr><td width="30%"><label> certificate </label></td> <td width="90%"><select name="certificate" width="90%">
                                                                                                         <option value="U">U</option>
                                                                                                         <option value="A">A</option>
                                                                                                         <option value="U/A">U/A</option>
                                                                                                         <option value="S">S</option>
                                                                                                         <option value="G">G</option>
                                                                                                         <option value="PG">PG</option>
                                                                                                         <option value="PG-13">PG-13</option>
                                                                                                         <option value="R">R</option>
                                                                                                         <option value="NC-17">NC-17</option>

                                                                                                        </select>
                                                                                                     </td></tr>
        </table>
        <input type="submit" value="UPLOAD" id="submit">
    </g:form>

    <g:link controller = "movie" action ="home">
    <button> << BACK</button> </g:link>

</div>
<div class="prop ${hasErrors(bean:movie,field:'title', 'errors')}">

</div>

<g:hasErrors bean="${movie}">
  <ul>
   <g:eachError var="err" bean="${movies}">
       <li><g:message error="${err}" /></li>
   </g:eachError>
  </ul>


</g:hasErrors>



</body></html>