<!DOCTYPE html>

<html xmlns:g="http://www.w3.org/1999/html"><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>Online Movie DataBase</title>
    <style>
body {
    background: url('http://webneel.com/daily/sites/default/files/images/daily/01-2016/25-the-nut-job-2-animation-movie-list-2016.jpg') no-repeat fixed center center;
    background-size: cover;
    font-family: Montserrat;
}


.movie-block {
    width: 89%;
    height: 700px;
    padding: 20px;
    background: #fff;
    border-radius: 5px;
    border-top: 5px solid #ff656c;
    border-bottom: 5px solid #ff656c;
    margin: 20px auto;
    opacity: 0.9;
}


.movie-block button {
    width: 15%;
    height: 40px;
    background: #ff656c;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #e15960;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 14px;
    font-family: Montserrat;
    outline: none;
    cursor: pointer;


}

#submit {
    width: 11%;
    height: 10px;
    background: #ff656c;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #e15960;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 14px;
    font-family: Montserrat;
    outline: none;
    cursor: pointer;
    margin-left:20;
    float: right;


</style>
</head>

<body>


<div class="movie-block">
    <h1>-------LIST OF MOVIE RECORD-------</h1>

    <table border="1px" bgcolor="#f1f1c1">
        <thead>
            <th>#ID</th>
            <th>TITLE</th>
            <th>GENRE</th>
            <th>DIRECTOR</th>
            <th>YEAR</th>
            <th>SUMMARY</th>
            <th>CERTIFICATE</th>
        </thead>
        <tbody>

        <g:each var="movieD" in="${movieData}">
            <tr>
                <td>${movieD.id}</td>
                <td>${movieD.title}</td>
                <td>${movieD.genre}</td>
                <td>${movieD.director}</td>
                <td>${movieD.year}</td>
                <td>${movieD.summary}</td>
                <td>${movieD.certificate}</td>
            </tr>
        </g:each>
        </tbody>
    </table>

    <br><g:link controller = "movie" action ="home"> <button> << BACK</button> </g:link> <br>
    <br><g:link controller = "movie" action ="addMovie"> <button > ADD movie</button> </g:link>

</div>




</body></html>