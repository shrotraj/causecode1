<!DOCTYPE html>

<html xmlns:g="http://www.w3.org/1999/html"><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>Online Movie DataBase</title>
    <style>
body {
    background: url('http://webneel.com/daily/sites/default/files/images/daily/01-2016/25-the-nut-job-2-animation-movie-list-2016.jpg') no-repeat fixed center center;
    background-size: cover;
    font-family: Montserrat;
}


.movie-block {
    width: 800px;
    height: 500px;
    padding: 20px;
    background-color:#B2BABB;
    border-radius: 5px;
    border-top: 5px solid #ff656c;
    border-bottom: 5px solid #ff656c;
    margin: 50px auto;
    font-family: Lucida Console;
    opacity: 0.9;
}


.movie-block button {
    width: 15%;
    height: 50px;
    background: #ff656c;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #e15960;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 14px;
    font-family: Montserrat;
    outline: none;
    cursor: pointer;


}



</style>
</head>

<body>


<div class="movie-block">
    <h1>-------REQUESTED EDIT LIST-------</h1>

    <table border="1px" bgcolor="#f1f1c1">
        <thead>
        <th>#ID</th>
        <th>TITLE</th>

        </thead>
        <tbody>

        <g:each var="editRequestMovie" in="${requestedMovies}">
            <tr>
                <td>${editRequestMovie.movie.id}</td>
                <td>${editRequestMovie.movie.title}</td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <br><g:link controller = "movie" action ="home"> <button> << BACK</button> </g:link> <br>
    <br><g:link controller = "movie" action ="requestPage"> <button > submit another requested</button> </g:link>

</div>




</body></html>