package moviedb.movie

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.web.databinding.GrailsWebDataBinder
import moviedb.EditRequest
import moviedb.Movie

import moviedb.RateRequest
import moviedb.User
import org.apache.tomcat.jni.User


@Secured(['ROLE_COMMON'])
class MovieController {

    SpringSecurityService springSecurityService
    GrailsWebDataBinder grailsWebDataBinder

    def addMovie() {}

    def home() {}

    def update(Movie movie) {
        println(params)

        println '>>>>>>>>>>>>>>>>>>>>>>>' + movie

        if(movie) {
            movie.save(flush: true)
            if (movie.errors) {
                movie.errors.allErrors.each {
                    println it
                }
            }
        }

        redirect(controller: 'movie', action: 'listMovies')

    }

    def index() {
        println(params)
        Map movieRecord = [title: params.title, genre: params.genre, director: params.director, year: params.year, summary: params.summary, certificate: params.certificate]
        Movie movieData = new Movie(movieRecord)
        if (!movieData.save()) {
            movieData.errors.allErrors.each {
                println it
            }
        }
        respond([success: true]);


        redirect(controller: "movie", action: "listMovies")
    }

    def listMovies() {

        List movieData = Movie.findAll()

        [movieData: movieData]

    }

    def requestPage() {

        List movieData = Movie.findAll()

        [movieData: movieData]
    }

    def updateList() {

        List movieData = Movie.findAll()

        [movieData: movieData]
    }

    def request() {

        println("=====${params}")

        Movie movieInstance = Movie.findById(params.edit)

        if (movieInstance) {

            EditRequest editRequest = new EditRequest()


            editRequest.movie = movieInstance
            editRequest.username = springSecurityService.currentUser.username


            if (!editRequest.save()) {
                editRequest.errors.allErrors.each {
                    println it
                }
            }

            redirect(controller: "movie", action: "requested")

        }


    }

    def requested() {

        List requestedMovies = EditRequest.findAll()

        [requestedMovies: requestedMovies]


    }

    def rateMovies() {

        List movieData = Movie.findAll()

        [movieData: movieData]

    }

    def rate1() {
        println("=====${params}")

        Movie movieInstance = Movie.findById(params.edit)

        if (movieInstance) {

            Movie rateRequest = new Movie()


            rateRequest.title = movieInstance.title
            //rateRequest.username = springSecurityService.currentUser.username
            rateRequest.rating = params.rating


            if (!rateRequest.save()) {
                rateRequest.errors.allErrors.each {
                    println it
                }
            }

            redirect(controller: "movie", action: "home")


        }

    }


    def delete(){
        println("=====${params}")

        Movie movieInstance = Movie.findById(params.edit)

        if (movieInstance) {

            RateRequest rateRequest = RateRequest.findByMovie(movieInstance)
            EditRequest editRequest = EditRequest.findByMovie(movieInstance)
            editRequest?.delete(flush: true)
            rateRequest?.delete(flush: true)
            movieInstance.delete(flush:true)
            }

            redirect(controller: "movie", action: "requested")
        }

    def toUpdate(){
        Movie movieData = Movie.findById(params.edit)

        render (view: 'update', model: [movieData: movieData])

    }

    def filter1(){
        def movieData = Movie.findAllByTitle(params.filter1)

        render(view:'filtered',model: [movieData: movieData])
    }

    def filter2(){
        def movieData = Movie.findAllByYear(params.filter2)

        render(view:'filtered',model: [movieData: movieData])
    }
    def filter3(){
        def movieData = Movie.findAllByCertificate(params.filter3)

        render(view:'filtered',model: [movieData: movieData])
    }

}


