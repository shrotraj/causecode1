<!DOCTYPE html>

<html xmlns:g="http://www.w3.org/1999/html"><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>Online Movie DataBase</title>
    <style>
body {
     background: url('http://webneel.com/daily/sites/default/files/images/daily/01-2016/25-the-nut-job-2-animation-movie-list-2016.jpg') no-repeat fixed center center;
    background-size: cover;
    font-family: Montserrat;
}


.home-block {
    width: 500px;
    height:500px;
    padding: 20px;
    background-color:#B2BABB;
    border-radius: 5px;
    border-top: 5px solid #ff656c;
    border-bottom: 5px solid #ff656c;
    margin: 50px auto;
    opacity: 0.9;
}


.home-block button {
    width: 55%;
    height: 10%;
    background: #ff656c;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #e15960;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 14px;
    font-family: Montserrat;
    outline: none;
    cursor: pointer;
    margin-left:20%;
}



</style>
</head>

<body>


<div class="home-block">

    <br> <g:link controller = "movie" action ="addMovie"> <button> Add movie record </button> </g:link> <br>
    <br> <g:link controller = "movie" action ="listMovies"> <button> List movies </button> </g:link> <br>
        <br> <g:link controller = "movie" action ="requestPage"> <button> Request for delete </button> </g:link> <br>
        <br> <g:link controller = "movie" action ="updateList"> <button> Request for update </button> </g:link> <br>
        <br> <g:link controller = "movie" action ="requested"> <button> Requested edits </button> </g:link> <br>
        <br> <g:link controller = "movie" action ="rateMovies"> <button> Rate a movie </button> </g:link> <br>
        <br><g:link controller='logout'><button>Logout</button></g:link><br>
</div>




</body></html>