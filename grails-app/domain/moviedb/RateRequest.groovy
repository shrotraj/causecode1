package moviedb

/**
 * Created by shrot on 24/6/17.
 */
class RateRequest {
    String username
    Movie movie
    int rating
    static constraints = {
    }
}
