<!DOCTYPE html>

<html xmlns:g="http://www.w3.org/1999/html"><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>Online Movie DataBase</title>
    <style>
body {
    background: url('http://webneel.com/daily/sites/default/files/images/daily/01-2016/25-the-nut-job-2-animation-movie-list-2016.jpg') no-repeat fixed center center;
    background-size: cover;
    font-family: Montserrat;
}

.logo {
    font-size:18px;
    text-align: center;
    width: 800px;
    height: 55px;
    background-color:white;
    border-radius: 5px;
    border-top: 5px solid #ff656c;
    border-bottom: 5px solid #ff656c;
    margin: 20px auto;
    opacity: 0.9;
       }

.login-block {
    width: 320px;
    padding: 20px;
    border-radius: 5px;
    border-top: 5px solid #ff656c;
    border-bottom: 5px solid #ff656c;
    margin: 50px auto;
    background-color:#B2BABB;
    opacity: 0.9;
}

.login-block h1 {
    text-align: center;
    color: #000;
    font-size: 18px;
    text-transform: uppercase;
    margin-top: 0;
    margin-bottom: 20px;
}

.login-block input {
    width: 100%;
    height: 42px;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #ccc;
    margin-bottom: 20px;
    font-size: 14px;
    font-family: Montserrat;
    padding: 0 20px 0 50px;
    outline: none;
}



.login-block button {
    width: 43%;
    height: 40px;
    background: #ff656c;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #e15960;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 14px;
    font-family: Montserrat;
    outline: none;
    cursor: pointer;
}

#submit {
    width: 43%;
    height: 40px;
    background: #ff656c;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #e15960;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 14px;
    font-family: Montserrat;
    outline: none;
    cursor: pointer;
    margin-left:20;
}

</style>
</head>

<body>

<div class="logo"><h3>Welcome to online Movie Database </h3></div>
<div class="login-block">
    <g:link controller="login" action="auth">
 <button>Login</button>
    </g:link>
    <g:link controller="login" action="registerUser">
        <button>Register</button>
    </g:link>

    <br>
<div>
    <sec:ifNotGranted roles="ROLE_USER">
      <facebookAuth:connect />
    </sec:ifNotGranted>
    <sec:ifAllGranted roles="ROLE_USER">
      Welcome <sec:username/>! (<g:link uri="/j_spring_security_logout">Logout</g:link>)
    </sec:ifAllGranted>
</div>
</br>
</div>
</body>
</html>