package moviedb

class Movie {

        String title
        String genre
        String director
        int    year
        String summary
        String certificate
        String rating

    static constraints = {

            title    size: 3..100, blank: false, unique: true
            genre    size: 3..100, blank: false
            director size: 3..100, blank: false
            year     size: 4,blank:false
            summary  size: 1..800, blank: false
            certificate   size:1..50, blank: true
            rating   size:1..50,blank:true,nullable: true

    }
}
