package moviedb

import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@TestFor(moviedb.ScreenController)
class ScreenControllerSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "testing screen controller options"() {
        when:"when user succesfully logs in"
        controller.commonPage()
        then: 'user should be redirected to common page view'
        view == '/screen/common'
    }
}